﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ComplexNumsOperators.Models
{
    public struct ComplexNumber
    {
        public double Real { get; set; }
        public double Imaginary { get; set; }
        public ComplexNumber(double real, double imaginary)
        {
            Real = real;
            Imaginary = imaginary;
        }
        public static ComplexNumber operator +(ComplexNumber a) => +a;
        public static ComplexNumber operator -(ComplexNumber a) => new ComplexNumber(-a.Real, -a.Imaginary);
        public static ComplexNumber operator +(ComplexNumber a, ComplexNumber b)
        {
            return new ComplexNumber
            {
                Real = a.Real + b.Real,
                Imaginary = a.Imaginary + b.Imaginary
            };
        }
        public static ComplexNumber operator -(ComplexNumber a, ComplexNumber b)
        {
            return new ComplexNumber
            {
                Real = a.Real - b.Real,
                Imaginary = a.Imaginary - b.Imaginary
            };
        }
        public static ComplexNumber operator *(ComplexNumber a, ComplexNumber b)
        {
            double x1 = a.Real * b.Real;
            double x2 = a.Real * b.Imaginary;
            double x3 = a.Imaginary * b.Real;
            double x4 = a.Imaginary * b.Imaginary * -1;

            return new ComplexNumber
            {
                Real = x1 + x4,
                Imaginary = x2 + x3
            };
        }
        public static ComplexNumber operator /(ComplexNumber a, ComplexNumber b)
        {
            var chisl = Math.Pow(b.Real, 2) + Math.Pow(b.Imaginary, 2);

            double x1 = a.Real * b.Real;
            double x2 = a.Real * b.Imaginary;
            double x3 = a.Imaginary * b.Real;
            double x4 = a.Imaginary * b.Imaginary * -1;

            return new ComplexNumber
            {
                Real = x1 + x4 / chisl,
                Imaginary = x2 + x3 / chisl
            };
        }

    }
}
