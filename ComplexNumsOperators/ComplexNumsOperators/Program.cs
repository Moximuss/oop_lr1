﻿using System;
using System.Collections.Generic;
using ComplexNumsOperators.Models;
using ComplexNumsOperators.Operations;
using static System.Console;

namespace ComplexNumsOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = new List<ComplexNumber>();
            var services = new ComplexNumsServices();

            while (true)
            {
                WriteLine(" 1 - Create complex number\n 2 - Find summary\n 3 - Find subtraction\n 4 - Find multiplication\n 5 - Find division\n 6 - Print complex numbers\n 7 - Close program");
                string input = ReadLine();
                switch (input)
                {
                    case "1":
                        {
                            WriteLine("Enter real number for first number");
                            double real = Convert.ToDouble(ReadLine());
                            WriteLine("Enter imagionary number for second number");
                            double imagionary = Convert.ToDouble(ReadLine());
                            numbers.Add(services.Create(real, imagionary));
                            break;
                        }
                    case "2":
                        {
                            for (int i = 0; i < numbers.Count - 1; i++)
                            {
                                services.Sum(numbers[i], numbers[i + 1]);
                            }
                            break;
                        }
                    case "3":
                        {
                            for (int i = 0; i < numbers.Count - 1; i++)
                            {
                                services.Subtraction(numbers[i], numbers[i + 1]);
                            }
                            break;
                        }
                    case "4":
                        {
                            for (int i = 0; i < numbers.Count - 1; i++)
                            {
                                services.Multiplication(numbers[i], numbers[i + 1]);
                            }
                            break;
                        }
                    case "5":
                        {
                            for (int i = 0; i < numbers.Count - 1; i++)
                            {
                                services.Division(numbers[i], numbers[i + 1]);
                            }
                            break;
                        }
                    case "6":
                        {
                            WriteLine("Complex numbers:");
                            for (int i = 0; i < numbers.Count; i++)
                            {
                                WriteLine(services.Print(numbers[i]));
                            }
                            break;
                        }
                    case "7":
                        {
                            Environment.Exit(1);
                            break;
                        }
                    default:
                        {
                            WriteLine("Enter 1 - Create two complex numbers, 2 - Find summary, 3 - Find subtraction, 4 - Find multiplication, 5 - Find division , 6 - Print complex numbers 7 - Close program");
                            break;
                        }
                }
            }
        }
    }
}
