﻿using System;
using System.Collections.Generic;
using System.Text;
using ComplexNumsOperators.Models;

namespace ComplexNumsOperators.Operations
{
    public class ComplexNumsServices
    {
        public ComplexNumber Create(double x, double y)
        {
            return new ComplexNumber(x, y);
        }

        public ComplexNumber Sum(ComplexNumber a, ComplexNumber b)
        {
            return new ComplexNumber
            {
                Real = a.Real + b.Real,
                Imaginary = a.Imaginary + b.Imaginary
            };
        }
        public ComplexNumber Subtraction(ComplexNumber a, ComplexNumber b)
        {
            return new ComplexNumber
            {
                Real = a.Real - b.Real,
                Imaginary = a.Imaginary - b.Imaginary
            };
        }
        public ComplexNumber Multiplication(ComplexNumber a, ComplexNumber b)
        {
            double x1 = a.Real * b.Real;
            double x2 = a.Real * b.Imaginary;
            double x3 = a.Imaginary * b.Real;
            double x4 = a.Imaginary * b.Imaginary * -1;

            return new ComplexNumber
            {
                Real = x1 + x4,
                Imaginary = x2 + x3
            };
        }

        public ComplexNumber Division(ComplexNumber a, ComplexNumber b)
        {
            var chisl = Math.Pow(b.Real, 2) + Math.Pow(b.Imaginary, 2);

            double x1 = a.Real * b.Real;
            double x2 = a.Real * b.Imaginary;
            double x3 = a.Imaginary * b.Real;
            double x4 = a.Imaginary * b.Imaginary * -1;

            return new ComplexNumber
            {
                Real = x1 + x4 / chisl,
                Imaginary = x2 + x3 / chisl
            };
        }

        public string Print(ComplexNumber number)
        {
            return $"{number.Real} + {number.Imaginary}i";
        }
    }
}